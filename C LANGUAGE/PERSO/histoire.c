#include <stdio.h>
/* Jeu d'entrainements aux dates importantes : Histoire de France, Inventions, etc */


int main(void){
    startGame();
}

int startGame(void){
    _Bool isStart = 0;
    printf("-----------------------\nBienvenue dans History\n-----------------------\nTaper 1 pour continuer...\n----------------------\n > ");
    scanf("%d", &isStart);
    if(isStart == 1){
        menuGame();
    } else {
        startGame();
    }
    return 0;
}

int menuGame(void){
    int menuUserChoice = 0;
    printf("\n---------------------\nMenu PRINCIPAL\n---------------------\n1. Commencer une nouvelle partie\n---------------------\n2. Options\n---------------------\n3. Plus d'infos\n---------------------\n4. Quitter le jeu\n---------------------\n> ");
    scanf("%d", &menuUserChoice);
}