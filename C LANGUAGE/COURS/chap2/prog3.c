#include <stdio.h>

int main(void)
{
    int age;
    printf("Quel age avez-vous ? > ");
    scanf("%d", &age);
    printf("Vous avez %d an(s)\n", age);
    return 0;
}