#include <stdio.h>

int main(void)
{
    printf("Quelques sauts de ligne\n\n\n");
    printf("\tIl y a une tabulation avant moi !\n");
    printf("Je voulais dire que ... \r");
    printf("Vous pourriez me laissez parler !\n");
    return 0;
}