#include <stdio.h>

int main(void)
{
    int x, y;
    printf("> ");
    scanf("%d %d", &x, &y);
    printf("x = %d | y = %d\n", x, y);
    return 0;
}