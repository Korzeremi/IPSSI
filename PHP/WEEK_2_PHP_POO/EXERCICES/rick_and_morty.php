<?php
    class Personnage{
        private $userChoice;

        public function __construct(){
            $this->userChoice="";
        }
        public function getUserChoice(){
            return $this->userChoice;
        }
        public function setUserChoice($choiceOfUser){
            $this->userChoice = $choiceOfUser;
        }

    }

    class Objets{
        private $userChoice1;
        private $userChoice2;

        public function __construct($uC1,$uC2){
            $this->userChoice1 = $uC1;
            $this->userChoice2 = $uC2;
        }
        public function getUC1(){
            return $this->userChoice1;
        }
        public function setUC1($userC1){
            $this->userChoice1 = $userC1;
        }
        public function getUC2(){
            return $this->userChoice2;
        }
        public function setUC2($userC2){
            $this->userChoice2 = $userC2;
        }
    }

    class NinNinNin extends Objets{
        private $portalgun;
        private $failed;

        public function __construct($PG,$FD){
            $this->portalgun = $PG;
            $this->failed = $FD;
        }
        public function getPortalGun(){
            return $this->portalgun;
        }
        public function setPortalGun($portalVar){
            $this->portalgun = $portalVar;
        }
        public function getFailed(){
            return $this->failed;
        }
        public function setFailed($failedVar){
            $this->failed = $failedVar;
        }
    }

    $Perso = new Personnage(" "," ");
    $Objet = new Objets(" "," ");
    $NinNinNin = new NinNinNin(" "," ");

    $portalInfos = " ";
    $failedInfos = " ";

    function userSelection($Perso,$Objet,$NinNinNin){

        $Perso->getUserChoice();
        echo "Veuillez choisir un personnage en tapant son nom :\n- Rick\n- Morty\n- Jerry\n";
        $currentCharacterChoice = strtolower((string)readline("> "));
        $Perso->setUserChoice($currentCharacterChoice);
        echo "\nPersonnage sélectionné : " . $Perso->getUserChoice() . "\n\n";

        $Objet->getUC1();
        echo "Veuillez choisir un premier objet :\n- Pistolet\n- MeesseeksBox\n- szechuanSauce\n";
        $object1Choice = strtolower((string)readline("> "));
        $Objet->setUC1($object1Choice);
        echo "\nPremier objet sélectionné : " . $Objet->getUC1() . "\n\n";

        $Objet->getUC2();
        echo "Veuillez choisir un deuxième objet :\n- pistolet\n- meesseeksBox\n- szechuanSauce\n";
        $object2Choice = strtolower((string)readline("> "));
        $Objet->setUC2($object2Choice);
        echo "\nSecond objet sélectionné : " . $Objet->getUC2() . "\n\n";

    }

    function comparingSystem($Perso,$Objet,$NinNinNin){
        switch($Perso->getUserChoice()){
            case "rick":
                rickSystem($Perso,$Objet,$NinNinNin);
                break;
            case "morty":
                mortySystem($Perso,$Objet,$NinNinNin);
                break;
            case "jerry":
                jerrySystem($Perso,$Objet,$NinNinNin);
                break;
        }

    }

    function rickSystem($Perso,$Objet,$NinNinNin){
        if(($Objet->getUC1() == "pistolet" and $Objet->getUC2() == "szechuansauce")or($Objet->getUC2() == "pistolet" and $Objet->getUC1() == "szechuansauce")){
            echo "\n\nça y est j'ai révolutionné l'univers\n\n";
            $NinNinNin->getPortalGun();
            $portalInfos = "P03tAlGuN";
            $NinNinNin->setPortalGun($portalInfos);
            echo $NinNinNin->getPortalGun();
        } else {
            echo "\n\nFailed\n\n";
        }
    }

    function mortySystem($Perso,$Objet,$NinNinNin){
        if(($Objet->getUC1() == "szechuansauce" and $Objet->getUC2() == "meesseeksbox")or($Objet->getUC2() == "szechuansauce" and $Objet->getUC1() == "meesseeksbox")){
            echo "\n\nencore un nouveau gosse de l'espace !\n\n";
            $NinNinNin->getPortalGun();
            $portalInfos = "Jerry";
            $NinNinNin->setPortalGun($portalInfos);
            echo $NinNinNin->getPortalGun();
        } else {
            echo "\n\nFailed\n\n";
        }
    }

    function jerrySystem($Perso,$Objet,$NinNinNin){
        $NinNinNin->getPortalGun();
        $portalInfos = "P03tAlGuN";
        $NinNinNin->setPortalGun($portalInfos);
        $NinNinNin->getFailed();
        $failedInfos = "JerryAgain";
        $NinNinNin->setFailed($failedInfos);
        echo $NinNinNin->getPortalGun() . " and " . $NinNinNin->getFailed();
    }

    function gameManager($Perso,$Objet,$NinNinNin){
        userSelection($Perso,$Objet,$NinNinNin);
        comparingSystem($Perso,$Objet,$NinNinNin);
    }

    gameManager($Perso,$Objet,$NinNinNin);

?>