<?php
//Creating the class GestionPersonnes
class GestionPersonnes {

    //Declaring the variables used to stock data
    public $nom;
    public $prenom;
    public $age;
    public $sexe;
    public $departement;
    public $adresse;

    //CONSTRUCTOR
    public function __construct($N,$P,$A,$S,$D, $F){
        $this->nom=$N;
        $this->prenom=$P;
        $this->age=$A;
        $this->sexe=$S;
        $this->departement=$D;
        $this->adresse=$F;
        //Calling display function with variables called in
        $this->display($N,$P,$A,$S,$D,$F);
    }

    //GET FUNCTIONS
    public function getNom(){
        return $this->nom;
    }
    public function getPrenom(){
        return $this->prenom;
    }
    public function getAge(){
        return $this->age;
    }
    public function getSexe(){
        return $this->sexe;
    }
    public function getDepartement(){
        return $this->departement;
    }

    //SET FUNCTIONS
    public function setNom($NewNom){
        $this->nom = $NewNom;
    }
    public function setPrenom($NewPrenom){
        $this->prenom = $NewPrenom;
    }
    public function setAge($NewAge){
        $this->age = $NewAge;
    }
    public function setSexe($NewSexe){
        $this->sexe = $NewSexe;
    }
    public function setDepartement($NewDepartement){
        $this->departement = $NewDepartement;
    }

    //Functions display asking for the adresse to the user in the CLI and showing the sentence with the variables
    public function display($N,$P,$A,$S,$D,$F){
        $F = (string)readline("> ");
        echo "La personne " . $N . " " . $P . " agé de " . $A . " ans et de sexe " . $S . " est né dans le " . $D . " habite à " . $F;
    }


}

//Instantiate the class
$Personne = new GestionPersonnes("Rémi", "KORZENIOWSKI", "20", "HOMME", "78", "R");

?>