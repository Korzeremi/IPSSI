<?php

//Rémi KORZENIOWSKI
/* ------------------------------------------------------------------------------------*/

/* Creating the 'Couleurs' Class */
class Couleurs{
    
    /* Private typed variables to dent modifications */
    private $color1;
    private $randomNb;
    private $colorsArray = array();
    private $userArray = array();

    /* Creating the constructor in public because it returns errors in public */
    public function __construct($c1,$nb,$cA,$uA){
        $this->color1=$c1;
        $this->randomNb=$nb;
        $this->colorsArray=$cA;
        $this->userArray=$uA;
    }

    /* Public typed functions because it returns errors in Private
    Getting the variable '$color1' ... */
    public function getC1(){
        return $this->color1;
    }
    public function getRandomNb(){
        return $this->randomNb;
    }
    public function getArray(){
        return $this->colorsArray;
    }
    public function getUserArray(){
        return $this->userArray;
    }
    public function setUserArray($uArray){
        $this->userArray = $uArray;
    }
    /* ... and setting the value of '$co1' to it */
    public function setC1($co1){
        $this->color1 = $co1;
    }
    public function setRandomNb($random){
        $this->randomNb = $random;
    }
    public function setArray($cArray){
        $this->colorsArray = $cArray;
    }

}

/* ------------------------------------------------------------------------------------*/

/* Creating the 'Peintre' Class which extends (include parameters and options) of 'Couleurs' Class */
class Peintre extends Couleurs{
    private $davinci; //Private typed variable
}

/* ------------------------------------------------------------------------------------*/

/* Initiate 'Couleurs' Class with arguments */
$Program = new Couleurs("TEST", "TEST2", "TEST3", "COLOR", "ARRAY", "USERARRAY");

/* gameManager function that manages the game by calling functions, asking user for colors and comparing */
function gameManager($Program){
    generateArraysColors($Program); //function generating colors and putting them in array
    $Program->getUserArray(); //getting userArray variable
    $userArray = array(); //creating a array for user
    $missingArray = array(); //creating a array for typed colors
    for($i=0; $i<3; $i++){ //for type loop for asking and comparing colors three times
        $userGuessColor = (string)readline("Taper les couleurs > "); //asking user for color to guess
        $userArray[] = $userGuessColor; //adding typed color to the array of user
        if($userGuessColor != $Program->getArray($i)){  //adding missing colors to the array
            $missingArray[] = $userGuessColor;
        }
        if(in_array($userGuessColor,$Program->getArray())){ //if colors are in the leonard's array
            echo "\nMagnifique peinture, dommage qu’il vous reste encore vos 2 oreilles\n";
        }else{
            echo("\nCouleur ajouter à la liste manquante\n");
            echo "\nVa faire les courses, la Joconde n'attend pas !\n";
        }
        }
    echo "\nCouleurs tapées :\n\n";
    print_r($missingArray);
    echo "\n\n----------------------------------------\n\nListes couleurs manquantes :\n\n";
    print_r($Program->getArray());
}


gameManager($Program); //calling gameManager function

/* generateRandomNumber function to generate random numbers */
function generateRandomNumber($Program){
    $Program->getRandomNb();
    $randomInteger = rand(0,7);
    $Program->setRandomNb($randomInteger);
}

/* generateColors function calling generateColors function to generate random numbers and attributing them colors */
function generateColors($Program){
   generateRandomNumber($Program);
   $Program->getC1();
   switch ($Program->getRandomNb()){
        case 0 :
            $test = "Red";
            $Program->setC1($test);
            break;
        case 1 :
            $test = "Orange";
            $Program->setC1($test);
            break;
        case 2 :
            $test = "Yellow";
            $Program->setC1($test);
            break;
        case 3 :
            $test = "Green";
            $Program->setC1($test);
            break;
        case 4 :
            $test = "Blue";
            $Program->setC1($test);
            break;
        case 5 :
            $test = "Purple";
            $Program->setC1($test);
            break;
        case 6 :
            $test = "Brown";
            $Program->setC1($test);
            break;
        case 7 :
            $test = "Pink";
            $Program->setC1($test);
            break;
   }
   $Program->getC1();
}

/* generateArraysColors function generate the array of colors needed */
function generateArraysColors($Program){
    $Program->getArray();
    $finalArray = array();
    for($i=0;$i<3;$i++){
        generateColors($Program);
        $finalArray[$i] = $Program->getC1();
    }
    $Program->setArray($finalArray);
}

/* ------------------------------------------------------------------------------------*/

?>