<?php
    //Rémi KORZENIOWSKI
    //Creating the class 'Starter'
    class Starter{
        //Private typed variables used to deny modifications
        private $pokeName; //pokeName variable used for pokémon's name (SALAMECHE ...)
        private $pokeType; //pokeType variable used for pokémon's type (FEU...) 
        private $pokeBehaviour; //pokeType variable used for pokémon's behaviour (généreux...)

        /* Constructor constructs our three variables and called the getPokemon() function */
        /* Public functions because it doesn't work in private */
        public function __construct($pN, $pT, $pH){
            $this->pokeName=$pN;
            $this->pokeType=$pT;
            $this->pokeBehaviour=$pH;
            $this->getPokemon();
        }
        /* Getting the variable "pokeName" ... */
        public function getName(){
            return $this->pokeName;
        }
        /* ... and setting the value as "bName*/
        public function setName($bName){
            $this->pokeName = $bName;
        }
        /* --- */
        public function getType(){
            return $this->pokeType;
        }
        /* -- */
        public function setType($pType){
            $this->pokeType = $pType;
        }
        /* -- */
        public function getBehave(){
            return $this->pokeBehaviour;
        }
        /* -- */
        public function setBehave($pBehave){
            $this->pokeBehaviour = $pBehave;
        }

        /* getPokemon() function asks a integer to the user that represents a pokémon and set the final pokémon */
        public function getPokemon(){
            echo "\nMerci de choisir un starter pokemon en tapant le numéro :\n- 1. Salameche\n- 2. Carapuce \n- 3. Pikachu\n ";
            $pName = (int)readline("> ");
            $this->getName();
            $this->setName($pName);
            $this->pokeInfos(); //calling pokeInfos() function to display infos
        }

        /* randomBehaviour makes a random number and set a random behaviour */
        public function randomBehaviour(){
            $rdmBehave = rand(1,3); //setting a random number between 1 and 3 to the '$rdmBehave' variable
            switch ($rdmBehave){ //Switch case, if random number equals a number, setting a specific behaviour
                case 1 :
                    $this->getBehave();
                    $fBehave = "flemmard";
                    $this->setBehave($fBehave); //Getting 'pokeBehaviour' variable and setting "flemmard" to it
                    break;
                case 2 :
                    $this->getBehave();
                    $fBehave = "joyeux";
                    $this->setBehave($fBehave);
                    break;
                case 3 :
                    $this->getBehave();
                    $fBehave = "genereux";
                    $this->setBehave($fBehave);
                    break;
                }
        }

        /* pokeInfos() function displays specific informations about the selected pokémon */
        public function pokeInfos(){
            $this->randomBehaviour(); //calling randomBehaviour() function to have a random behaviour
            switch ($this->getName()){ //switch case, if name equals a specific number, displaying specific infos of it
                case 1 :
                    $this->getType();
                    $fType = "FEU";
                    $this->setType($fType);
                    echo "Salameche type feu et " . $this->getBehave() . ": LA MAISON BRULE";
                    break;
                case 2 :
                    $this->getType();
                    $fType = "EAU";
                    $this->setType($fType);
                    echo " Carapuce type eau et " . $this->getBehave() . ": Encore une fuite...";
                    break;
                case 3 :
                    $this->getType();
                    $fType = "ELECTRICITE";
                    $this->setType($fType);
                    echo " Pikachu type électricité et " . $this->getBehave() . ": Ma batterie portable est enfin pleine !";
                    break;
                default: //if $this->getName() != 1 or 2 or 3, diplays that :
                    echo "ERROR";
                    break;
            }
        }
    }

    //Calling the class
    $Starter = new Starter("Pokemon", "Type", "Behavihour", "TEST");

