<?php

//Creating a new class
class GestionPersonnes {

    /*Definition of several public variables used to stock data
    Public type variables to called them everywhere in the code*/
    public $nom;
    public $prenom;
    public $age;
    public $sexe;
    public $departement;

    /*GET type functions to return variables called*/ 
    public function getNom(){
        return $this->nom;
    }
    public function getPrenom(){
        return $this->prenom;
    }
    public function getAge(){
        return $this->age;
    }
    public function getSexe(){
        return $this->sexe;
    }
    public function getDepartement(){
        return $this->departement;
    }

    /*SET type functions to set */
    public function setNom($NewNom){
        $this->nom = $NewNom;
    }
    public function setPrenom($NewPrenom){
        $this->prenom = $NewPrenom;
    }
    public function setAge($NewAge){
        $this->age = $NewAge;
    }
    public function setSexe($NewSexe){
        $this->sexe = $NewSexe;
    }
    public function setDepartement($NewDepartement){
        $this->departement = $NewDepartement;
    }

}

//Instantiate class GestionPersonnes and getting and setting variables
$Personne = new GestionPersonnes();
$Personne->getNom();
$Personne->setNom("KORZENIO...TROP LONG");
$Personne->getPrenom();
$Personne->setPrenom("REMI");
$Personne->getAge();
$Personne->setAge(20);
$Personne->getSexe();
$Personne->setSexe("HOMME");
$Personne->getDepartement();
$Personne->setDepartement("78");

$Personne2 = new GestionPersonnes();
$Personne2->getNom();
$Personne2->setNom("ROMERO");
$Personne2->getPrenom();
$Personne2->setPrenom("RAPH");
$Personne2->getAge();
$Personne2->setAge(21);
$Personne2->getSexe();
$Personne2->setSexe("HOMME");
$Personne2->getDepartement();
$Personne2->setDepartement("77");

//Showing data from clases to the user
echo "La personne ", $Personne->getNom(), " ", $Personne->getPrenom(), " agé de ", $Personne->getAge(), "ans est de sexe ", $Personne->getSexe(), " et est nee dans le ", $Personne->getDepartement();
echo "\nLa personne ", $Personne2->getNom(), " ", $Personne2->getPrenom(), " agé de ", $Personne2->getAge(), "ans est de sexe ", $Personne2->getSexe(), " et est nee dans le ", $Personne2->getDepartement();

?>