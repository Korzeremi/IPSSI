<?php
// Rémi KORZENIOWSKI

/* Creating "TriangleRectangle" class */
    class TriangleRectangle{
        /* Declaring variables 
        Private variables typed to secure it */
        private $longueur; //Used for the data of the length typed by the user
        private $largeur; //Used for the data of the width typed by the user
        private $result; //Used for the data of the calculation of the hypotenuse

        /* Constructor constructs our three objects variables 
        Public variables typed used because errors if protected or private used */
        public function __construct($lo,$la,$r){
            $this->longueur=$lo;
            $this->largeur=$la;
            $this->result=$r;
        }

        /* Getting the variable "largeur"... */
        public function getLarg(){
            return $this->largeur;
        }
        /* ... and setting the value as "$newLa" */
        public function setLarg($newLa){
            $this->largeur = $newLa;
        }
        /* Getting the variable "longueur"... */
        public function getLong(){
            return $this->longueur;
        }
        /* ... and setting the value as "$newLo" */
        public function setLong($newLo){
            $this->longueur = $newLo;
        }
        /* Getting the variable "result"... */
        public function getResult(){
            return $this->result;
        }
        /* ... and setting the value as "$hypo" */
        public function setResult($hypo){
            $this->result = $hypo; // $Hypo will be used into calculHypo() function to display the hypotenuse */
        }
        /* CalculHypo() function with two parameters for the length and the width */
        public function calculHypo($lo, $la){
            $Final = ($lo)**2 + ($la)**2; //Calculation of the square hypotenuse stocked into '$Final' variable
            $hypo = sqrt($Final); //Calculation of the final hypotenuse stocked into '$hypo' variable
            $this->getResult(); //Getting the 'result' variable ...
            $this->setResult($hypo); //...and setting it as '$hypo' value (so the hypotenuse)
        }
    }
    /* Calling class 'TriangleRectangle' with its parameters */
    $TR = new TriangleRectangle(1, 1, "hypotenuse");

    /* Asking value for length and width to the user in the prompt command*/
    $longChoice = (int)readline("Longueur > ");
    $largChoice = (int)readline("Largeur > ");

    /* Setting length and width with user's datas typed... */
    $TR->setLarg($largChoice);
    $TR->setLong($longChoice);
    /* ...and getting values for length and width */
    $TR->getLarg();
    $TR->getLong();

    /* Calling the calculHypo() function : it'll calculate the hypotenuse with '$longChoice' and 'largChoice' values*/
    $TR->calculHypo($longChoice, $largChoice);
    /* Displaying class to the user in the command prompt*/
    print_r($TR);
?>