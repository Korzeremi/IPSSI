<?php
    /* Creating the "Recrue" class */
    class Recrue
    {
        /* Declaring the variables used to stock data, public types to used them everywhere in the code */
        public $nom;
        public $prenom;
        public $age;
        public $insigne;
        public $nvDePrivilege;

        /* Constructor */
        public function __construct($N,$P,$A,$I,$D){
            $this->nom=$N;
            $this->prenom=$P;
            $this->age=$A;
            $this->insigne=$I;
            $this->nvDePrivilege=$D;
        }

        /* Get function picks variables and attributes them*/
        public function getNom(){
            return $this->nom;
        }
        /* Setting variables to new one to modify them*/
        public function setNom($NewNom){
            $this->nom = $NewNom;
        }
        /* SAME */
        public function getPrenom(){
            return $this->prenom;
        }
        /* SAME */
        public function setPrenom($NewPrenom){
            $this->prenom = $NewPrenom;
        }

        /* SAME */
        public function getAge(){
            return $this->age;
        }
        /* SAME */
        public function setAge($NewAge){
            $this->age = $NewAge;
        }
        
        /* SAME */
        public function getInsigne(){
            return $this->insigne;
        }
        /* SAME */
        public function setInsigne($NewInsigne){
            $this->insigne = $NewInsigne;
        }
        
        /* SAME */
        public function getNvDePrivilege(){
            return $this->nvDePrivilege;
        }
        /* SAME */
        public function setNvDePrivilege($NewNvDePrivilege){
            $this->nvDePrivilege = $NewNvDePrivilege;
        }

    }

    
    /* Creating the "Marin" class who took Recrue content withh extends parameter */
    class Marin extends Recrue
    {
        public $arme;
        public $insigne;
        public $nvDePrivilege;
        
        /* New constructor importing parent's one */
        public function __construct($N, $P, $A, $I, $D){
            /* Calling the parent's constructor */
            parent::__construct($N,$P,$A,$I,$D);
            $this->arme="Poisson";
            $this->insigne="Insigne bleu";
            $this->nvDePrivilege=1;
            /* Calling function to display infos*/
            $this->displayMarin($N, $P, $A);
        }
        
        /* Displaying infos to the user */
        public function displayMarin($N, $P, $A){
            echo "Marin : " . $N . " " . $P . " " . $A . " " . $this->arme;
        }
    }

    /* Creating the militaire class who took Recrue content with extends parameter*/
    class Militaire extends Recrue
    {
        public $arme;
        public $insigne;

        public $nvDePrivilege;
        
        
        public function __construct($N, $P, $A, $I, $D){
            /* Calling the parent's constructor */
            parent::__construct($N,$P,$A,$I,$D);
            $this->arme="Mitraillette";
            $this->insigne="Insigne vert";
            $this->nvDePrivilege=1;
            /* Calling function to display infos*/
            $this->displayMarin($N, $P, $A);
        }
        
        /* Displaying infos to the user */
        public function displayMarin($N, $P, $A){
            echo "Marin : " . $N . " " . $P . " " . $A . " " . $this->arme;
        }
    }

    // $I = "Aucun Insi gne";
    // $D = "Nivea&& 80u de privilège = 0";
    // echo "\nnom : " . $N . "\nprénom : " . $P . "\nâge : " . $A . " ans\ninsigne : " . $I . "\nniveau de privilège : " . $D . "\n\n";
    
    /* Instatiate severals classes Recrue, Militaire and Marine */
    $Recrue = new Recrue("Nom", "Prenom", "Age", "Aucun insigne", "0");
    /* Asking users for the name, firstname and age */
    $N = (string)readline("Taper le nom de la nouvelle recrue > ");
    $P = (string)readline("Taper le prenom de la nouvelle recrue > ");
    $A = (string)readline("Taper le age de la nouvelle recrue > ");
    $Recrue->getNom();
    $Recrue->setNom($N);
    $Recrue->getPrenom();
    $Recrue->setPrenom($P);
    $Recrue->getAge();
    $Recrue->setAge($A);
    $Marin = new Marin($Recrue->getNom(), $Recrue->getPrenom(), $Recrue->getAge(), "a", "e");
    $Militaire = new Militaire($Recrue->getNom(), $Recrue->getPrenom(), $Recrue->getAge(), "a", "e");
    print_r($Recrue);
    print_r($Marin);
    print_r($Militaire);

    
    // /* Creating the "Militaire" class */
    // class Militaire
    // {
        
        // }
        
        ?>