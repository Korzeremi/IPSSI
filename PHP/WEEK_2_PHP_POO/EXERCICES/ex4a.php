<?php
    /* Creating the "Recrue" class */
    class Recrue
    {
        /* Declaring the variables used to stock data */
        public $nom;
        public $prenom;
        public $age;
        public $insigne;
        public $nvDePrivilege;

        /* Constructor */
        public function __construct($N,$P,$A,$I,$D){
            $this->nom=$N;
            $this->prenom=$P;
            $this->age=$A;
            $this->insigne=$I;
            $this->nvDePrivilege=$D;
            $this->display($N,$P,$A,$I,$D);
        }

        public function getNom(){
            return $this->nom;
        }
        public function setNom($NewNom){
            $this->nom = $NewNom;
        }

        public function getPrenom(){
            return $this->prenom;
        }
        public function setPrenom($NewPrenom){
            $this->prenom = $NewPrenom;
        }
        
        public function getAge(){
            return $this->age;
        }
        public function setAge($NewAge){
            $this->age = $NewAge;
        }
        
        public function getInsigne(){
            return $this->insigne;
        }
        public function setInsigne($NewInsigne){
            $this->insigne = $NewInsigne;
        }
        
        public function getNvDePrivilege(){
            return $this->nvDePrivilege;
        }
        public function setNvDePrivilege($NewNvDePrivilege){
            $this->nvDePrivilege = $NewNvDePrivilege;
        }

        private function display($N,$P,$A,$I,$D){
            $N = (string)readline("Taper le nom de la nouvelle recrue > ");
            $P = (string)readline("Taper le prénom > ");
            $A = (int)readline("Taper votre age > ");
            $I = "Aucun Insigne";
            $D = "Niveau de privilège = 0";
            echo "\nnom : " . $N . "\nprénom : " . $P . "\nâge : " . $A . " ans\ninsigne : " . $I . "\nniveau de privilège : " . $D . "\n\n";
        }

    }

    $Recrue = new Recrue("Nom", "Prenom", "Age", "Insigne", "nvDePrivilege");

    /* Creating the "Marin" class */
    class Marin 
    {
        protected $arme;

        private function __construct(){
            
        }
    }

    // /* Creating the "Militaire" class */
    // class Militaire
    // {

    // }

?>