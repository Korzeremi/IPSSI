<?php

class GestionPersonnes {

    public $nom;
    public $prenom;
    public $age;
    public $sexe;
    public $departement;

    //CONSTRUCTOR
    public function __construct($N,$P,$A,$S,$D){
        $this->nom=$N;
        $this->prenom=$P;
        $this->age=$A;
        $this->sexe=$S;
        $this->departement=$D;

        $this->display($N,$P,$A,$S,$D);
    }

    //GET FUNCTIONS
    public function getNom(){
        return $this->nom;
    }
    public function getPrenom(){
        return $this->prenom;
    }
    public function getAge(){
        return $this->age;
    }
    public function getSexe(){
        return $this->sexe;
    }
    public function getDepartement(){
        return $this->departement;
    }

    //SET FUNCTIONS
    public function setNom($NewNom){
        $this->nom = $NewNom;
    }
    public function setPrenom($NewPrenom){
        $this->prenom = $NewPrenom;
    }
    public function setAge($NewAge){
        $this->age = $NewAge;
    }
    public function setSexe($NewSexe){
        $this->sexe = $NewSexe;
    }
    public function setDepartement($NewDepartement){
        $this->departement = $NewDepartement;
    }

    //Function used to showing infos to the user
    public function display($N,$P,$A,$S,$D){
        echo "La personne " . $N . " " . $P . " agé de " . $A . " ans et de sexe " . $S . " est né dans le " . $D;
    }

}

//Instantiate class GestionPersonnes
$Personne = new GestionPersonnes("Rémi", "KORZENIOWSKI", "20", "HOMME", "MELUN");

?>