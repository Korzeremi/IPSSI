<!-- This program is printing even numbers  -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function evenNumbers(){
    
        //Type for loop showing even number if it is
        for($i=0; $i<=100; $i++){
            if ($i % 2 == false){
                echo("$i \n");
            }
        }

    }

    evenNumbers();
?>