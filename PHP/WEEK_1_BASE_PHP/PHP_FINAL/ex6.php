<!-- This program is sorting numbers  -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function sorting(){
    
        // We determine the length of the array and we create the array
        $lengthTab = (int)readline("Please type the number of values to sort > ");
        $userArray = array();

        // Type for loop that add typed values to the array
        for($i=0; $i<$lengthTab; $i++){
            $firstArray = (int)readline("Can you type the value > ");
            array_push($userArray, $firstArray);
        }
        
        // We sort the array and we print it
        sort($userArray);
        print_r($userArray);

    }

    sorting();
?>