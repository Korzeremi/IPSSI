<!-- This program is making the sum of the number typing by user -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function sumOfUserTypedSequence(){
    
        // Asking for the number to do the sum
        $typedNumber = (int)readline("Please can you type the number to make the sum of it > ");
        // Value of the sum at the end
        $finalNumber = 0;

        // Type for loop that makes the sum
        for ($i = 0; $i < $typedNumber + 1; $i ++){

            $finalNumber = $finalNumber + $i;
        }
        
        echo($finalNumber);
        
    }

    sumOfUserTypedSequence();
?>