<!-- This program is verifying if two strings are anagrams -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function areStringsTypedAnagram(){
    
        // Asking user to type two strings
        $firstString = (string)readline("Can you type the first string > ");
        $secondString = (string)readline("Can you type the second string > ");
        
        // We transform our strings into arrays
        $firstStringArray = str_split($firstString);
        $secondStringArray = str_split($secondString);
        
        // We sort arrays
        sort($firstStringArray);
        sort($secondStringArray);

        // Type switch loop verifying if they're anagrams
        switch (true){
            case (strlen($firstString) == strlen($secondString)) and $firstStringArray == $secondStringArray:
                echo("Anagrams");
                break;
            default :
                echo("Not anagrams");
                break;
        }

    }

    areStringsTypedAnagram();
?>