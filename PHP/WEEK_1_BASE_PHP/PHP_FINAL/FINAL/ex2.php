<!-- This program is reversing the string typed by user -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function reverseStringOfUserTypedSequence(){

        // $typedString is asking the user items to reverse
        $typedString = (string)readline("Please can you type items to reverse > ");
        // $typedStringLength is the length of $typedString in int
        $typedStringLength = strlen($typedString);
        // Type for loop that start from the end of the sequence and show us each caracter from the end to the start of the typed sequence
        for ($i=($typedStringLength+1); $i>=0; $i --){
            echo($typedString[$i]);
        }        
    }

    reverseStringOfUserTypedSequence();
?>