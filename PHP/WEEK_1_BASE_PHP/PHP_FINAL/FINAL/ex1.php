<!-- This program shows us the biggest number -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function maxNumberOfUserTypedSequence(){

        // We instantiate $userArray that creates an array for our user's values
        $userArray = array();
        // We ask the user the number of values to analysis 
        $lenghtUserArray = (int)readline("Please can you type the number of values to analysis > ");
    
        // Type for loop asking for a value to add to $userArray thanks to $lenghtUserArray number
        for($i = 0; $i < $lenghtUserArray; $i ++){
            $arrayValues = (int)readline("Please can you type the value to analysis > "); // Asking for the current value to add
            array_push($userArray, $arrayValues); // Adding the asking value to the array
        }
        
        // We instantiate $maxValueOfUserArray that will be the maximum number
        $maxValueOfUserArray = 0;
    
        // Type for loop searching for the maximum value of $userArray
        for($i=0; $i < $lenghtUserArray; $i ++){
            // If current number in the array is bigger than the maximum number, update the max number
            if($userArray[$i] > $maxValueOfUserArray){
                $maxValueOfUserArray = $userArray[$i];
            }
        }
    
        // Show in terminal the max number
        echo("The maximum number of the sequence is $maxValueOfUserArray");

    }

    maxNumberOfUserTypedSequence();
?>