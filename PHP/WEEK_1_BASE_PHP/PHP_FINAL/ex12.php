<!-- This program is creating converting euro in USD, GBP or CAN  -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php

    function moneyConversion(){
        $startValue = (int)readline("Please enter the value in euro you want to convert > ");
        $moneyType = (string)readline("Please chose between USD, GBP and CAN > ");

        switch (true){
            case ($moneyType == "USD"):
                $convertValue = $startValue / 1 * 1.20;
                echo("$convertValue $ USA");
                break;
            case ($moneyType == "GBP"):
                $convertValue = $startValue / 1 * 0.88;
                echo("$convertValue Livres Sterlings");
                break;
            case ($moneyType == "CAN"):
                $convertValue = $startValue / 1 * 1.60;
                echo("$convertValue $ CANADA");
                break;
            default:
                echo("Empty");
                break;
        }
    }

    moneyConversion();

?>