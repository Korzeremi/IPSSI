<!-- This program is printing the sul between 0 and 1000  -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php
    function thousandSum(){

        $sumThousand = 0;
    
        for($i = 1; $i <= 1000; $i++){
            switch (true){
                case ($i % 3 == 0):
                    $sumThousand += $i;
                    break;
                case ($i % 5 == 0):
                    $sumThousand += $i;
                    break;
                default:
                    break;
            }
        }
    
        echo($sumThousand);

    }

    thousandSum();
?>