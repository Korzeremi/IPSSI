<!-- This program is creating a random string  -->
<!-- Rémi KORZENIOWSKI's restricted program ! -->
<?php

    function randomString(){

        // Random Length for the final string
        $stringLength = rand(4, 20);
        // Charsets for string
        $chars = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN';
        $charsLength = strlen($chars);
        // Final string
        $finalString = "";
        //Type for loop that pick a random charset for a random string length and display it
        for ($i = 0; $i < $stringLength; $i++){
            $finalString = $chars[rand(0, $charsLength -1)];
            echo($finalString);
        }

    }

    randomString();
?>