<?php
    $age = (int)readline("Bienvenue chez la MEUF : Mutuelle Elementaire Utilitaire Française. Merci de taper votre âge : ");
    $permis = (int)readline("Merci de taper vos années de permis : ");
    $nombreAccidents = (int)readline("Merci de taper votre nombre d'accidents : ");
    $anneesAdhesions = (int)readline("Depuis combien de temps êtes avec la MEUF : ");
    $statusMEUF = 0;
    // 0 = BLEU, 1 = VERT, 2 = ORANGE, 3 = ROUGE
    switch (true){
        case ($age <= 25 and $age >= 18 and $permis <= 2 and $nombreAccidents == 0):
            $statusMEUF = 3;
            break;
        case ($age <= 25 and $age >= 18 and $permis > 2 and $nombreAccidents == 0) or ($age > 25 and $permis <= 2 and $nombreAccidents == 0):
            $statusMEUF = 2;
            break;
        case ($age <= 25 and $age >= 18 and $permis > 2 and $nombreAccidents == 1) or ($age > 25 and $permis <= 2 and $nombreAccidents == 1):
            $statusMEUF = 3;
            break;
        case ($age > 25 and $permis > 2 and $nombreAccidents == 0):
            $statusMEUF = 1;
            break;
        default :
            echo("PAS A MOI ! T'AS PAS LE PERMIS B*T*RD");
            break;
    }

    if($anneesAdhesions >= 5){
        switch ($statusMEUF){
            case 3:
                $statusMEUF = 2;
                break;
            case 2:
                $statusMEUF = 1;
                break;
            case 1:
                $statusMEUF = 0;
                break;    
        }
    }

    switch ($statusMEUF){
        case 0:
            $status = "Tarif MEUF ROUGE";
            break;
        case 1:
            $status = "Tarif MEUF ORANGE";
            break;
        case 2:
            $status = "Tarif MEUF VERT";
            break;
        case 3:
            $status = "Tarif MEUF BLEU";
            break;
        default:
            $status = "PAS ASSURABLE";
            break;
    }

    echo($status);
?>