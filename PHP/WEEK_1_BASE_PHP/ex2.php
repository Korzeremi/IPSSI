<?php 
  $hour = readline("Merci de taper l'heure : ");
  $minutes = readline("Merci de taper les minutes : ");
  $secondes = readline("Merci de taper les secondes : ");
  $secondes += 1;
  if($secondes >= 59){
    $secondes = "00";
    $minutes += 1;
  }
  if($minutes > 59){
    $minutes = "00";
    $hour += 1;
  }
  if($hour > 23){
    $hour = "00";
  }
  echo "Dans une seconde, il sera $hour h $minutes et $secondes secondes";
?>