<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body{
            background-color: black;
            background-size: 800px 450px;
            color: white;
            text-shadow: 0px 0px 1px red;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .textTyping{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-top: 20px;
        }

        .radioSelection{
            margin-top: 10px;
            margin-left:10px;
        }

        .cardDescription{
            margin-top: 20px;
            margin-bottom: 15px;
            resize: vertical;
            box-shadow: 0px 0px 20px black;
        }

        .name{
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .descr{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .generate {
            margin-top: 10px;
            margin-left: 45px;
        }

        .cardName{
            box-shadow: 0px 0px 20px black;
        }

        .title{
            text-shadow: 0px 0px 20px black;
        }

    </style>
</head>
<body>
    <audio autoplay loop>
        <source src="yutheme.mp3">
    </audio>
    <div class="title">SUPER YUGIOH GENERATOR 2000</div>
    <div class="app">
        <form action="yugiohProcess.php" method="POST"> <!-- Link with the PHP FILE -->
            <div class="textTyping">
                <div class="name">
                    Nom de la carte : <input type="text" class="cardName" name="cardName" required>
                </div>
                <div class="descr">
                    Décrire la carte : <textarea class="cardDescription" name="cardDescription" required></textarea>
                </div>
            </div>
            <div class="other">
                <div class="radioSelection">
                    Monstre<input type="radio" name="cardType" value="Monstre">
                    Magie<input type="radio" name="cardType" value="Magie">
                    Piège<input type="radio" name="cardType" value="Piège">
                    Default<input type="radio" name="cardType" value="Default" checked="checked">
                </div>
                <div class="generate">
                    <button class="generate">Générer la carte</button>
                </div>
            </div>
        </form>
    </div>
</body>