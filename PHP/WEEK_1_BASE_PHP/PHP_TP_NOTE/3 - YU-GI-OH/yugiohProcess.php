<style>
    body{
        background-color: black;
        color: white;
    }

</style>

<body>
    <audio autoplay loop>
        <source src="yutheme2.mp3">
    </audio>
    <form action="yugiohMain.php">
        <h3>Carte ajoutée avec succès</h3>
        <div class="home">
            <button class="homeBtn">Home</button>
        </div>
    </form>
</body>

<!-- PHP area -->
<?php
//Definition of our variables
$cardName = $_POST['cardName']; //$cardName variable takes the value of the 'cardName'  name in the HTML file linked 
$cardDescr = $_POST['cardDescription']; //$cardDescr variable takes the value of the 'cardDescription'  name in the HTML file linked
$cardType = $_POST['cardType']; //$cardType variable takes the value of the 'cardType'  name in the HTML file linked
$cardChars = '/^[a-zA-Z._ ]{1,30}$/'; //$cardChars set our allowed characters

if(preg_match($cardChars, $cardName)){ //Verifying the $cardName variable with the characters saved in the $cardChars variable
    echo "Name valid ! "; //Displaying Name valid ! if the cardName variable has the correct characters
} else {
    echo "Name not valid, please type characters only ! ";
}

//Printing card Informations
echo "Nom de la carte : $cardName";
echo "</br>Description de la carte : $cardDescr";
echo "</br>Type de carte : $cardType";