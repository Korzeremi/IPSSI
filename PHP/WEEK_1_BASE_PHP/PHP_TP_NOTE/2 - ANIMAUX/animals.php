<?php
    //Int number which represents the user's choice when he is choosing the animal.
    $userChoice = 0;

    //listAnimals function prints main infos when the user starts the program as the program's name, the animal's list.
    //This function is asking the user to type a number to attribute the value to the $userChoice variable and it returns it.
    function listAnimals(){
        //$userChoice variable declaration as global to import it into the function.
        global $userChoice;
        //Printing main infos when the program starts.
        echo("----------------------------------------------\nADIT (Animals Database Information Technology)");
        echo("\nMerci de bien vouloir choisir un animal en tapant le numéro correspondant :");
        echo("\n1.Lion \n2.Tigre \n3.Panda \n4.Ours \n5.Singe \n");
        //Attribute the typed value by the user to the $userChoice.
        $userChoice = (int)readline("> ");
        //Return $userChoice value to use it out of the function.
        return $userChoice;
    }

    //showAnimalInfo function is using $userChoice value to print information about the selected animal.
    function showAnimalInfo($userChoice){
        //switch conditions type used to print specific contents with a specific value of $userChoice.
        switch ($userChoice){ //$userChoice as parameter for the switch.
            case 1 : //first case : if $userChoice = 1, do :
                echo("\nLe Lion :\n");
                echo("\nHabitat :\nSavanes de l'Afrique subsaharienne ainsi que la forêt de Gir en Inde pour la sous-espèce asiatique.\n");
                echo("\nRégime alimentaire :\nCarnivore.\n");
                echo("\nHabitudes de vie :\n'Le Roi de la savane' a pour habitude de vivre en clan. Ce clan se compose généralement de lionnes, de jeunes sujets et d'un mâle dominant.\n");
                break; //if the condition is not valid, don't execute it
            case 2 : //second case : if $userChoice = 2, do :
                echo("\nLe Tigre :\n");
                echo("\nHabitat :\nJungle tropicale sur le continent asiatique : Inde, Indochine, Chine, Sibérie, Sumatra.\n");
                echo("\nRégime alimentaire :\nCarnivore.\n");
                echo("\nHabitudes de vie :\nLe tigre est une félin solitaire et n'aime pas beaucoup partager son territoire. Il se repose dans ses tanières disposées sur son territoire.\nLes femelles s'occupent des petits tigrons.\n");
                break; //if the condition is not valid, don't execute it
            case 3 : //third case : if $userChoice = 3, do :
                echo("\nLe Panda :\n");
                echo("\nHabitat :\nCentre de la Chine dans les montagnes dans les grandes forêts.\n");
                echo("\nRégime alimentaire :\nHerbivore.\n");
                echo("\nHabitudes de vie :\nIls passent leur vie à manger des bambous et à se promener en forêt\nIls sont de bons grimpeurs et peuvent aussi nager.\n");
                break; //if the condition is not valid, don't execute it
            case 4 : //fourth case : if $userChoice = 4, do :
                echo("\nL'Ours :\n");
                echo("\nHabitat :\nL'ours vit généralement en montagne ou au coeur des forêts dans une tanière. Région : Asie, Amérique du Nord, Europe.\n");
                echo("\nRégime alimentaire :\nOmnivore.\n");
                echo("\nHabitudes de vie :\nAnimal surtout nocturne et crépusculaire mais il est plus diurne là où il n'est pas dérangé. Il est assez craintif.\n");
                break; //if the condition is not valid, don't execute it
            case 5 : //fifth case : if $userChoice = 5, do :
                echo("Le Singe :");
                echo("\nHabitat :\nforêts tropicales , bois tempérés, savanes, parfois prairies et deserts. Région : Afriques, Asie.\n");
                echo("\nRégime alimentaire :\nOmnivore.\n");
                echo("\nHabitudes de vie :\nAnimal arboricole, ils peuvent vivre en communautés parfois jusqu'à plus de 100 individus.\n");
                break; //if the condition is not valid, don't execute it
            default: //default case : if $userChoice is different from 1 to 5, do :
                echo("NON DISPONIBLE");
                break; //if the condition is not valid, don't execute it
        }
    }

    listAnimals(); //calling function to execute it when the program starts
    showAnimalInfo($userChoice); //same
