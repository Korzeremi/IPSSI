<?php

//Function Game() used to manage the Game
function Game(){

    echo("\nBienvenue dans le jeu du pendu.\n"); //Displaying infos
    $wishWord = readline("Merci de taper un mot à deviner > "); //Attribute the word to wish to the $wishWord variable
    $wishWordLength = strlen($wishWord); //Attribute the length of the word to wish in the $wishWordLength variable in integer
    $uiArray = []; //Empty array with the future word to wish in
    $userArray = []; //Empty array for the typing word
    $lettersArray = []; //Empty array for typed letters
    $lifes = 9; //Number of lives
    
    for($i = 0 ; $i < $wishWordLength; $i++){ //For type loop to create a array full of "-".
       $userArray[$i] = "-"; //Adding to $userArray : "-"
    }
    
    for($i = 0; $i < $wishWordLength; $i++){ //For type loop to insert the word to wish in the $uiArray array
        $uiArray[$i] = $wishWord[$i]; //Adding the specific letter of the word in $wishWord to specific place in $uiArray array
    }
    
    while ($userArray != $uiArray){ //While the player don't know the full word
    
        if($lifes <= 0){ //If the life = 0
            echo("\nVous avez PERDU.\n\nLa solution était :\n\n");
            echo implode($uiArray); //Displaying the answer
            break;
        }
    
        echo("\n\nIl vous reste $lifes vies.\n");
        $answerLetter = readline("Taper une lettre > ");
        $isInit = False; //Know if the game verifying ?
        $i2 = 0;
    
        for ($i = 0; $i < $wishWordLength; $i++){ //Type for loop to compare typed letter to letter in current word to wish
    
            if($answerLetter == $uiArray[$i]){ //If the letter is in the word
                $isInit = True;
                $lettersArray[] = $answerLetter; //Add the letter to the array
                $userArray[$i] = $answerLetter; //Adding the letter to the specific place in the array
                $i2 += 1;
                echo("\nLettres utilisées :\n");
                echo implode($lettersArray);
                echo("\nMot : \n");
                echo implode($userArray);
            }
    
        }
    
        if ($isInit == False and $answerLetter != $uiArray[$i2]){ //If letter isn't in the word
            $lifes -= 1; //remove a life
            $lettersArray[] = $answerLetter;
            echo("\nLettres tapées : \n");
            echo implode($lettersArray);
        }
    
    }
    
    if($userArray == $uiArray){ //If the player knows the full word, you win.
        echo("\n\nVous avez gagné.\n");
    }
}

Game();