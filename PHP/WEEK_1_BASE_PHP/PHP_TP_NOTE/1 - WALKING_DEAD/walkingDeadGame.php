<?php
    //User choice variable return the number choosen by user to choose options
    $userChoice = 0;
    //Variable used in the while type loop (Game is on ?)
    $on = 1;
    //StartGame function renders the game when function is called 
    function startGame(){
        //Global variable to return and used it everywhere
        global $userChoice;
        //Displaying infos to the player
        echo("\nBienvenue dans The Walking Dead Game.\n");
        echo("\nVeuillez choisir une option en tapant le numéro correspondant.");
        echo("\n1. Se promener\n2. Chercher de la nourriture\n3. Trouver un abri\n4. Quitter le jeu\n");
        //Attributing the integer typed by the player to the $userChoice variable
        $userChoice = (int)readline("> ");
        echo("\n");
        //Switch type loop displaying random answer with the $userChoice variable typed by the user
        switch ($userChoice){
            //When $userChoice = 1, do
            case 1 :
                //Computer choose a random number between 0 and 2
                $randomNb = rand(0,2);
                switch ($randomNb){ 
                    //If $randomNb = 0
                    case 0 :
                        echo("Vous marchez pendant une heure et vous ne trouvez rien d'intéressant.");
                        break;
                    //If $randomNb = 1
                    case 1 :
                        echo("Vous tombez sur un groupe de zombies affamés. Vous réussissez à vous enfuir en courant.");
                        break;
                    //If $randomNb = 2
                    case 2 :
                        echo("Vous trouvez une boite de conserve abandonnée. Vous la prenez et continuez votre chemin.");
                        break;
                }
                break;
            //When $userChoice = 2, do
            case 2 :
                $randomNb = rand(0,2);
                switch ($randomNb){
                    case 0 :
                        echo("Vous fouillez les maisons et les magasins, mais ne trouvez rien à manger.");
                        break;
                    case 1 :
                        echo("Vous trouvez un jardin potager abandonné. Vous récoltez quelques légumes pour votre prochain repas.");
                        break;
                    case 2 :
                        echo("Vous tombez sur un groupe de zombies affamés alors que vous fouillez un supermarché. Vous réussissez à vous enfuir en courant, mais vous n'avez pas trouvé de nourriture.");
                        break;
                }
                break;
            //When $userChoice = 3, do
            case 3 :
                $randomNb = rand(0,2);
                switch ($randomNb){
                    case 0 :
                        echo("Vous trouvez une maison abandonnée et décidez de vous y installer pour la nuit.");
                        break;
                    case 1 :
                        echo("Vous cherchez un abri pendant des heures, mais ne trouvez rien de convenable.");
                        break;
                    case 2 :
                        echo("Vous tombez sur un groupe de survivants qui vous accueillent à bras ouverts dans leur abri.");
                        break;
                }
                break;
            //When $userChoice = 4, do
            case 4 :
                echo("\nAu revoir !\n");
                exit; //Quit the program
        }

    }

    //While $on = 1, the startGame() function is called, so the player can play the game as long as $userChoice != 4
    while($on = 1){
        startGame();
    }