<?php 
/* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* Creating the "Personnage" class */
    class Personnage{
    /* Declaration of variables in private to prevent modifications */        
        private $caraName;
        private $caraSexe;
        private $caraType;
        private $warrior;
        private $mage;
        private $archer;
        private $caraHealthPoints;
        private $caraDamage;
        private $caraLevel;
        private $potion;
        /* Creation of the constructor to instantiate variables */
        public function __construct($cN,$cS,$cT,$cHP,$cD,$cL){
            $this->caraName = $cN;
            $this->caraSexe = $cS;
            $this->caraType = $cT;
            $this->warrior = "Warrior";
            $this->mage = "Mage";
            $this->archer = "Archer";
            $this->caraHealthPoints = $cHP;
            $this->caraDamage = $cD;
            $this->caraLevel = $cL;
            $this->potion = 1;
        }
        /* Getter : Getting the variable from the constructor of the Class */
        public function getPotion(){
            return $this->potion;
        }
        public function getCaraDamage(){
            return $this->caraDamage;
        }
        public function getCaraHealthPoints(){
            return $this->caraHealthPoints;
        }
        public function getCaraLevel(){
            return $this->caraLevel;
        }
        public function getWarrior(){
            return $this->warrior;
        }
        public function getMage(){
            return $this->mage;
        }
        public function getArcher(){
            return $this->archer;
        }
        public function getCaraName(){
            return $this->caraName;
        }
        public function getCaraSexe(){
            return $this->caraSexe;
        }
        public function getCaraType(){
            return $this->caraType;
        }
        /* Setter : Setting the value of the variable returned of the constructor */
        public function setPotion($finalPotion){
            $this->potion = $finalPotion;
        }
        public function setCaraDamage($finalCaraDamage){
            $this->caraDamage = $finalCaraDamage;
        }
        public function setCaraHealthPoints($finalCaraHealthPoints){
            $this->caraHealthPoints = $finalCaraHealthPoints;
        }
        public function setCaraName($finalCaraName){
            $this->caraName = $finalCaraName;
        }
        public function setCaraLevel($finalCaraLevel){
            $this->caraLevel = $finalCaraLevel;
        }
        public function setCaraSexe($finalCaraSexe){
            $this->caraSexe = $finalCaraSexe;
        }
        public function setCaraType($finalCaraType){
            $this->caraType = $finalCaraType;
        }
    }
/* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* Creating the "Enemies" class */
    class Enemies{
        /* Declaration of variables in private to prevent modifications */        
        private $enSexe;
        private $enName;
        private $enHealthPoints;
        private $enDamage;
        private $enType;
        /* Creation of the constructor to instantiate variables */
        public function __construct($eN,$eS,$eHP,$eD,$eT){
            $this->enName = $eN;
            $this->enSexe = $eS;
            $this->enHealthPoints = $eHP;
            $this->enDamage = $eD;
            $this->enType = $eT;
        }
        /* Getter : Getting the variable from the constructor of the Class */
        public function getEnemiesArray(){
            return $this->enemiesStatus;
        }
        public function getEnType(){
            return $this->enType;
        }
        public function getEnSexe(){
            return $this->enSexe;
        }
        public function getEnName(){
            return $this->enName;
        }
        public function getEnDamage(){
            return $this->enDamage;
        }
        public function getEnHealthPoints(){
            return $this->enHealthPoints;
        }
        /* Setter : Setting the value of the variable returned of the constructor */
        public function setEnemiesArray($finalEnemiesArray){
            $this->enemiesStatus = $finalEnemiesArray;
        }
        public function setEnType($finalEnType){
            $this->enType = $finalEnType;
        }
        public function setEnSexe($finalEnSexe){
            $this->enSexe = $finalEnSexe;
        }
        public function setEnName($finalEnName){
            $this->enName = $finalEnName;
        }
        public function setEnDamage($finalEnDamage){
            $this->enDamage = $finalEnDamage;
        }
        public function setEnHealthPoints($finalEnHealthPoints){
            $this->enHealthPoints = $finalEnHealthPoints;
        }
    }
/* ----------------------------------------------------------------------------------------------------------------------------------------- */
    /* Instantiate the "Personnage" class ... */
    $PersoCS = new Personnage("caraName","caraSexe","caraType","caraHealthPoints","caraDamage","caraLevel"); // ... with its arguments
    /* Instantiate the "Enemies" class ... */
    $EnemiesCS = new Enemies("enemyName","enemySexe","enemyHealhPoints","enemyDamage","enemyType"); // ... with its arguments

    function startupAnimation(){
        $animation = array("[          ]","[R         ]","[Ré        ]","[Rém       ]","[Rémi      ]","[Rémi_     ]","[Rémi_G    ]","[Rémi_Ga   ]","[Rémi_Gam  ]","[Rémi_Game ]","[Rémi_Games]");
        for($i=0;$i<11;$i++){
            popen('cls','w');
            echo $animation[$i];
            usleep(50000);
            $timer = $i;
        }
    }


    /* gameStartupt() function manage the game when you start it */
    function gameStartup($PersoCS,$EnemiesCS){
        $introStatus = 0;
        startupAnimation();
        while($introStatus == 0){
            popen('cls','w');
            echo "\n\n-----------------------------------------\nBienvenue dans RPG (Rémi's Programmed Game)\n-----------------------------------------\n";
            popen('color c', 'w');
            sleep(1);
            popen('color e', 'w');
            sleep(1);
            $introStatus = readline("Appuyer sur Entrée...");
        }
        gameMenu($PersoCS,$EnemiesCS);
    }

    function gameMenu($PersoCS,$EnemiesCS){
        popen('cls','w');
        popen('color e','w');
        echo "\n\n-----------------------------------------\nRPG (Rémi's Programmed Game)\n-----------------------------------------\n\nJOUER (nouvelle partie)\nCHARGER (sauvegarde)\nEFFACER (sauvegarde)\nRESET (sauvegarde si corrompue)\nINFOS (...)\nQUITTER\n\n";
        $userMenuChoice = strtolower((string)readline("Taper le texte en gras SVP > "));
        switch($userMenuChoice){
            case "jouer":
                characterCreator($PersoCS,$EnemiesCS);
                fightManager($PersoCS,$EnemiesCS);
                break;
            case "charger":
                $fileSave = file('save.txt');
                if($fileSave == NULL){
                    popen('color c','w');
                    popen('cls','w');
                    echo "!!!! AUCUN FICHIER DE SAUVEGARDE A CHARGER : NON TROUVE OU CORROMPU !!!!";
                    sleep(4);
                    gameMenu($PersoCS,$EnemiesCS);
                } else {
                    $test = file('save.txt');
                    $PersoCS->setCaraName($test[0]);
                    $PersoCS->setCaraSexe($test[1]);
                    $PersoCS->setCaraType($test[2]);
                    $PersoCS->setCaraLevel($test[3]);
                    $PersoCS->setCaraHealthPoints($test[4]);
                    $PersoCS->setCaraDamage($test[5]);
                    $PersoCS->setPotion($test[6]);
                    popen('color a','w');
                    popen('cls','w');
                    echo "!!!! SAUVEGARDE DE " . $PersoCS->getCaraName() . " BIEN CHARGEE !!!!";
                    sleep(4);
                    fightManager($PersoCS,$EnemiesCS);
                }
                break;
            case "effacer":
                if($fileSave == NULL){
                    popen('color c','w');
                    popen('cls','w');
                    echo "!!!! AUCUN FICHIER DE SAUVEGARDE A EFFACER !!!!";
                    sleep(4);
                } else {
                    unlink('save.txt');
                    popen('color b','w');
                    popen('cls','w');
                    echo "!!!! SAUVEGARDE BIEN EFFACEE !!!!";
                    sleep(4);
                }
                gameMenu($PersoCS,$EnemiesCS);
                break;
            case "reset":
                file_put_contents('save.txt', NULL);
                popen('color b','w');
                popen('cls','w');
                echo "!!!! RESET SAUVEGARDE !!!!";
                sleep(4);
                gameMenu($PersoCS,$EnemiesCS);
                break;
            case "infos":
                echo "Quoi ?";
                sleep(2);
                echo 'FEUR !';
                usleep(15000);
                gameMenu($PersoCS,$EnemiesCS);
            case "quitter":
                echo "\n\nAU REVOIR !";
                exit;
            default:
                echo "ERROR";
                gameMenu($PersoCS,$EnemiesCS);
                break;
        }
    }


    /* characterCreator() manage the creation of our main character */
    function characterCreator($PersoCS,$EnemiesCS){

        /* Assignment of a name to the character */
        echo "\n\n----------------------------------\n";
        $PersoCS->getCaraName();
        $customCaraName = strtolower((string)readline("Merci de taper un nom pour ton personnage > "));
        while(($customCaraName == "")){ // Verification
            echo "\nNom invalide ! Merci de bien vouloir réessayer !\n";
            $customCaraName = strtolower((string)readline("Merci de taper un nom pour ton personnage > "));
        }
        echo "\nNom valide !\n\n";
        $PersoCS->setCaraName($customCaraName);

        /* Attribution of the gender to the character */
        $PersoCS->getCaraSexe();
        $customCaraSexe = strtolower((string)readline("Merci de taper le sexe pour ton personnage (femme/homme) > "));
        while(($customCaraSexe != "femme") && ($customCaraSexe != "homme")){ // Verification
            echo "\nSexe invalide ! Merci de bien vouloir réessayer !\n";
            $customCaraSexe = strtolower((string)readline("Merci de taper le sexe pour ton personnage (femme/homme) > "));
        }
        echo "\nSexe valide !\n";
        $PersoCS->setCaraSexe($customCaraSexe);

        /* Assignment of the class to the character with its specific parameters such as health and damage */
        echo "\nDans RPG, tu peux choisir entre trois classes de personnage :\n\n";
        echo " - Guerrier : 50 points de vie et 8 points de dégâts\n - Mage : 25 points de vie et 16 points de dégâts\n - Archer : 35 points de vie et 12 points de dégâts\n - Aléatoire\n\n";
        $PersoCS->getCaraType();
        $customCaraType = strtolower((string)readline("Choisis ta classe aventurier ! > "));
        while(($customCaraType != "guerrier") && ($customCaraType != "mage") && ($customCaraType != "archer") && ($customCaraType != "aléatoire")){ // Verification
            echo "\nClasse invalide ! Merci de bien vouloir réessayer !\n\n";
            $customCaraType = strtolower((string)readline("Choisis ta classe aventurier ! > "));
        }
        $PersoCS->setCaraType($customCaraType);
        $PersoCS->getCaraHealthPoints();
        $PersoCS->getCaraDamage();

        /* Setting up the class, damage and health to the character */
        switch($PersoCS->getCaraType()){
            case "guerrier":
                $PersoCS->getWarrior();
                $PersoCS->setCaraType($PersoCS->getCaraType());
                $PersoCS->setCaraDamage(8);
                $PersoCS->setCaraHealthPoints(50);
                break;
            case "mage":
                $PersoCS->getMage();
                $PersoCS->setCaraType($PersoCS->getCaraType());
                $PersoCS->setCaraDamage(16);
                $PersoCS->setCaraHealthPoints(25);
                break;
            case "archer":
                $PersoCS->getArcher();
                $PersoCS->setCaraType($PersoCS->getCaraType());
                $PersoCS->setCaraDamage(12);
                $PersoCS->setCaraHealthPoints(35);
                break;
            case "aléatoire":
                getRandomCharacter($PersoCS,$EnemiesCS); //function to pick up a random class
                break;
        }

        /* Definition of the player's level at 1*/
        $PersoCS->getCaraLevel();
        $PersoCS->setCaraLevel(1);

        /* Displaying current created character's informations to the user in the terminal */
        echo "\nClasse valide !\n";
        echo "\n-----------------------------------------\nVotre personnage a bien été créé !\n";
        echo "\nNom : " . $PersoCS->getCaraName() . "\nSexe : " . $PersoCS->getCaraSexe() . "\nClasse : " . $PersoCS->getCaraType() . "\nPoints de vie (PV) : ";
        echo $PersoCS->getCaraHealthPoints() . "\nPoint de dégâts (DP) : " . $PersoCS->getCaraDamage();
        echo "\n-----------------------------------------\n\n";
        echo "-----------------------------------------\n";
        echo $PersoCS->getCaraName() . ", vous êtes au niveau " . $PersoCS->getCaraLevel() . ".\n\n";
        readline("Appuer sur Entrée...");
    }


    /* getRandomCharacter() allows you to generate a random class */
    function getRandomCharacter($PersoCS,$EnemiesCS){
        $randomCaraNb = rand(0,2);
        switch($randomCaraNb){
            case 0:
                $customCaraType = "guerrier";
                $PersoCS->setCaraDamage(8);
                $PersoCS->setCaraHealthPoints(50);
                break;
            case 1:
                $customCaraType = "mage";
                $PersoCS->setCaraDamage(16);
                $PersoCS->setCaraHealthPoints(25);
                break;
            case 2:
                $customCaraType = "archer";
                $PersoCS->setCaraDamage(12);
                $PersoCS->setCaraHealthPoints(35);
                break;
        }
        $PersoCS->setCaraType($customCaraType);
    }

    /* fightManager() function manages the fight system */
    function fightManager($PersoCS,$EnemiesCS){
        popen('cls','w'); //Allows you to erase the terminal
        popen('color e', 'w');
        echo "\nNiveau " . $PersoCS->getCaraLevel(); 
        global $enemiesStatus;
        global $potion;
        $potion = 0;
        $enemiesStatus = randomEnemyCreator($PersoCS,$EnemiesCS);
        currentRound($PersoCS,$EnemiesCS,$enemiesStatus);
    }


    function nextRound($PersoCS,$EnemiesCS,$enemiesStatus){
        if(intval(sizeof($enemiesStatus))==0){
            popen('color a', 'w');
            echo "\n\nNIVEAU REUSSI \n\n";
            $actualCaraLevel = intval($PersoCS->getCaraLevel());
            $actualCaraLevel ++;
            $PersoCS->setCaraLevel($actualCaraLevel);
            $PersoCS->setCaraHealthPoints($PersoCS->getCaraHealthPoints()+3);
            $PersoCS->setCaraDamage($PersoCS->getCaraDamage()+2);
            echo ("\n!!!! Sauvegarde en cours !!!!\n\n");
            $saveData = $PersoCS->getCaraName() . "\n" . $PersoCS->getCaraSexe() . "\n" . $PersoCS->getCaraType() . "\n" . $PersoCS->getCaraLevel() . "\n" . $PersoCS->getCaraHealthPoints() . "\n" . $PersoCS->getCaraDamage() . "\n" . $PersoCS->getPotion();
            file_put_contents('save.txt', $saveData);
            readline("Appuyer sur Entrée...");
            fightManager($PersoCS,$EnemiesCS);
        }
    }


    function currentRound($PersoCS,$EnemiesCS,$enemiesStatus){
        popen('color e', 'w');
        popen('cls', 'w');
        echo "\nPrépare-toi à combattre !\n\nTaper le numéro de l'ennemi à sélectionner ";
        echo "\n" . $PersoCS->getCaraName() .", vous avez " . $PersoCS->getCaraHealthPoints() . " PV et " . $PersoCS->getCaraDamage() . " DP.\n";
        global $userEnemyChoice;
        showingEnemies($PersoCS,$EnemiesCS,$enemiesStatus);
        $userEnemyChoice = (int)readline("> ");
        lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
    }


    function showingEnemies($PersoCS,$EnemiesCS,$enemiesStatus){
        global $i;
        sleep(1);
        foreach(range(0,$PersoCS->getCaraLevel()-1) as $i){
            if($enemiesStatus[$i]!=NULL){
                echo "\n" . $enemiesStatus[$i]->getEnType() . " possède " . $enemiesStatus[$i]->getEnHealthPoints() . " points de vie et " . $enemiesStatus[$i]->getEnDamage() . " points de dégâts.\nSon numéro est " . $i . "\n";
            }else{
                echo " ";
            }
        }
    }


    /* lifesManager() function manages the entire game's life system */
    function lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice){
        popen('color e', 'w');
        switch(true){
            case($enemiesStatus[$userEnemyChoice]->getEnHealthPoints() <= 0):
                popen('color a', 'w');
                echo "\n\nEnnemi mort !\n";
                unset($enemiesStatus[$userEnemyChoice]);
                nextRound($PersoCS,$EnemiesCS,$enemiesStatus);
                currentRound($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                sleep(2);
                break;
            case($PersoCS->getCaraHealthPoints() <= 0):
                popen('color b', 'w');
                echo "\nVous êtes mort !";
                sleep(2);
                exit;
                break;
            case($enemiesStatus[$userEnemyChoice]->getEnHealthPoints() >= 0):
                currentFightManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice,$potion);
                break;
                }
    }


    function doDamageToYou($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice){
        popen('color b', 'w');
        popen('cls', 'w');
        $lostHealth = intval($PersoCS->getCaraHealthPoints()) - intval($enemiesStatus[$userEnemyChoice]->getEnDamage());
        $PersoCS->setCaraHealthPoints($lostHealth);
        echo "\nVous recevez des dégâts de " . $enemiesStatus[$userEnemyChoice]->getEnType() . " et êtes à présent à " . $PersoCS->getCaraHealthPoints() . "PV.";
        sleep(2);
    }

    function defenseSkills($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice){
        popen('cls', 'w');
        popen('color c', 'w');
        $defense = (intval($enemiesStatus[$userEnemyChoice]->getEnDamage())/2);
        $lostHealth = intval($PersoCS->getCaraHealthPoints()) - intval($defense);
        $PersoCS->setCaraHealthPoints($lostHealth);
        $skill = (intval($PersoCS->getCaraDamage())/2);
        $enemyLostHealth = intval($enemiesStatus[$userEnemyChoice]->getEnHealthPoints()) - intval($skill);
        $enemiesStatus[$userEnemyChoice]->setEnHealthPoints($enemyLostHealth);
        echo "\nVous utilisez votre compétences Défense !\nVous recevez " . $defense . " points de dégâts de " . $enemiesStatus[$userEnemyChoice]->getEnType() . ", êtes à présent à " . $PersoCS->getCaraHealthPoints() . " PV et infligez " . $enemyLostHealth . " points de dégâts à l'ennemi.";
        sleep(3);
    }



    /* currentFightManager() manages the actual enemies in the current round */
    function currentFightManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice,$potion){
        popen('cls','w'); //Allows you to erase the terminal
        echo "\n" . $PersoCS->getCaraName() .", vous avez " . $PersoCS->getCaraHealthPoints() . " PV et " . $PersoCS->getCaraDamage() . " DP.\n";
        echo "\nL'ennemi " . $enemiesStatus[$userEnemyChoice]->getEnType() . " possède " . $enemiesStatus[$userEnemyChoice]->getEnHealthPoints() . " PV et " . $enemiesStatus[$userEnemyChoice]->getEnDamage() . " DP.\n";
        echo "\n - Attaquer : vous attaquez un ennemi\n - Potion : vous rend 15 points de vie\n - Défense : divise par deux les dégâts reçus et renvoie des dégâts équivalent à la moitié de l'attaque du joueur\n\n";
        $userFightChoice = strtolower((string)readline("Que veux-tu faire ? (Taper le mot-clé) > "));
        switch($userFightChoice){
            case "attaquer":
                (int)$enemyFightStatus = intval($enemiesStatus[$userEnemyChoice]->getEnHealthPoints()) - intval($PersoCS->getCaraDamage());
                $enemiesStatus[$userEnemyChoice]->setEnHealthPoints($enemyFightStatus);
                if($enemiesStatus[$userEnemyChoice]->getEnHealthPoints() > 0){
                    doDamageToYou($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                }
                lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                break;
            case "potion":
                if($PersoCS->getPotion() == 1){
                    $PersoCS->setCaraHealthPoints($PersoCS->getCaraHealthPoints()+15);
                    $PersoCS->setPotion(0);
                    popen('color a','w');
                    echo "\nLa potion a été utilisée !\n";
                    sleep(2);
                    lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                } 
                if($PersoCS->getPotion() == 0){
                    popen('color c','w');
                    echo "\nLa potion a déjà été utilisée !\n";
                    sleep(2);
                    lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                }
                break;
            case "défense":
                if($enemiesStatus[$userEnemyChoice]->getEnHealthPoints() > 0){
                    defenseSkills($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                    lifesManager($PersoCS,$EnemiesCS,$enemiesStatus,$userEnemyChoice);
                }
                break;
            default:
                echo "ERROR";
                break;
        }
    }


    /* enemyCreator() allows you to generate various ENEMIES */
    function randomEnemyCreator($PersoCS,$EnemiesCS){
        $enemiesStatus = array();
        for($i=0;$i<=($PersoCS->getCaraLevel()-1);$i++){
            $randomEnemy = rand(0,4);
            $currentEnemy = new Enemies("","","","","");
            switch($randomEnemy){
                case 0:
                    $currentEnemy->setEnType("Dracula");
                    $currentEnemy->setEnDamage(5);
                    $currentEnemy->setEnHealthPoints(30);
                    $currentEnemy->setEnName("Dracula");
                    $currentEnemy->setEnSexe("M");
                    break;
                case 1:
                    $currentEnemy->setEnType("Miss Pacman");
                    $currentEnemy->setEnDamage(3);
                    $currentEnemy->setEnHealthPoints(15);
                    $currentEnemy->setEnName("Miss Pacman");
                    $currentEnemy->setEnSexe("F");
                    break;
                case 2:
                    $currentEnemy->setEnType("StarFox");
                    $currentEnemy->setEnDamage(6);
                    $currentEnemy->setEnHealthPoints(17);
                    $currentEnemy->setEnName("StarFox");
                    $currentEnemy->setEnSexe("M");
                    break;
                case 3:
                    $currentEnemy->setEnType("Goblin");
                    $currentEnemy->setEnDamage(2);
                    $currentEnemy->setEnHealthPoints(10);
                    $currentEnemy->setEnName("Goblin");
                    $currentEnemy->setEnSexe("M");
                    break;
                case 4:
                    $currentEnemy->setEnType("Ogre");
                    $currentEnemy->setEnDamage(8);
                    $currentEnemy->setEnHealthPoints(20);
                    $currentEnemy->setEnName("Ogre");
                    $currentEnemy->setEnSexe("M");
                    break;
            }
            array_push($enemiesStatus,$currentEnemy);
        }     
        return $enemiesStatus;
    }

    gameStartup($PersoCS,$EnemiesCS);   
// ----------------------------------------------------------------------------------------------------------------------------------------- */
?>